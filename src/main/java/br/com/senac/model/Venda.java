/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.model;

public class Venda {

    private String cliente;
    private double valorLivro;
    private String destino;

    public Venda(String cliente, double valorLivro, String destino) {
        this.cliente = cliente;
        this.valorLivro = valorLivro;
        this.destino = destino;
    }

    public double maranhaoV() {

        return valorLivro * 0.12;

    }

    public double saoPauloV() {

        double impostos = (valorLivro * 0.18);

        return impostos;
    }

    public double rioDeJaneiroV() {

        double imposto = (valorLivro * 0.17);

        return imposto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValorLivro() {
        return valorLivro;
    }

    public void setValorLivro(double valorLivro) {
        this.valorLivro = valorLivro;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

}
