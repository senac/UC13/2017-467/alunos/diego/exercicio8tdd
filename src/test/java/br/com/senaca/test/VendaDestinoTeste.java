package br.com.senaca.test;
import br.com.senac.model.Venda;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class VendaDestinoTeste {
    
    public VendaDestinoTeste() {
    }
   
    @Test
    public void CalcularImpostoPraSP(){
        
        Venda vendendoParaSaoPaulo = new Venda("Livraria da 25 de Março", 943, "SP" );
        assertEquals(169.74, vendendoParaSaoPaulo.saoPauloV() , 0.01);     
    }
        
    
     @Test
    public void CalcularImpostoPraRJ() {

        Venda vendendoParaRio = new Venda("Livraria da Rocinha ", 745, "RJ");
        assertEquals(126.65, vendendoParaRio .rioDeJaneiroV(), 0.01);
    }
    
    @Test
    public void CalcularImpostoPraMA(){
        Venda vendendoParaMaranhao = new Venda("Livraria do Nordeste Vote 17", 379 , "MA");
        assertEquals( 45.48 , vendendoParaMaranhao.maranhaoV() , 0.01 );    
    }


}
